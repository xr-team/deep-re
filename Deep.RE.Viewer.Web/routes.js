const express = require('express');
const path = require('path');

const router = express.Router();

router.get('/', function (req, res) {
    res.redirect('/1'); //todo: remove
})

router.get('/:id', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

module.exports = router;