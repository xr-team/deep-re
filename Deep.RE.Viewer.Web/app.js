var express = require('express');
var app = express();
var path = require('path');

require('hot-module-replacement')({
  ignore: /node_modules/
});

app.use(express.static('viewer'));

let router = require('./routes');
app.use('/', function(req, res, next) {
  router(req, res, next);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

if (module.hot) {
  module.hot.accept('./routes', () => {
    router = require('./routes');
  });
}