﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

public class CopyToWebApp
{
    [PostProcessBuild(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        Debug.Log(pathToBuiltProject);

        if (target == BuildTarget.WebGL)
        {
            string targetPath = Path.GetFullPath(Path.Combine(Application.dataPath, @"..\..\Deep.RE.Viewer.Web\viewer"));
            Directory.Delete(targetPath, true);

            string indexPath = Path.Combine(targetPath, @"..\index.html");
            if (File.Exists(indexPath)) File.Delete(indexPath);

            Copy(pathToBuiltProject, targetPath);
            File.Move(Path.Combine(targetPath, "index.html"), indexPath);
        }
    }

    static void Copy(string sourceDir, string targetDir)
    {
        Directory.CreateDirectory(targetDir);

        foreach (var file in Directory.GetFiles(sourceDir))
            File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));

        foreach (var directory in Directory.GetDirectories(sourceDir))
            Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
    }
}